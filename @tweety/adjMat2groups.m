function [gList, gStruct] = adjMat2groups(adjMat)

    % Calculates connected components of an adjacency matrix
    % 
    % Description:
    % ------------
    % Function calculates the connected components of an adjacency matrix
    % and sorts them by descending size.
    % The corresponding groups related to the connected components
    % are represented in two different formats for post-processing
    %
    % Syntax:
    % -------
    % [gList, gStruct] = adjMat2groups(adjMat)
    %
    % Variable:
    % ---------
    % adjMat  : [n n] adjacency matrix
    % gList   : [n 3] matrix with (objId, groupSize, groupIdx) in every row
    % gStruct : structure of sets representing groups correlated to connected components
    %
    % Method:
    % -------
    % Use Tarjan's algorithm to find strongly connected components and rearrange
    % them by descending size
    %
    % Example:
    % --------
    % n/a
    %
    % Literatur:
    % ----------
    % n/a
    %

    % use Matlab build-in function
    [S, C] = graphconncomp(sparse(adjMat));

    counts=histc(C,1:S);
    [~,idx]=sort(counts,'descend');
    [~,idxInv]=sort(idx);
    gList(:,1)=1:size(adjMat,1);
    gList(:,2)=counts(C);
    gList(:,3)=idxInv(C);

    for i=1:S
        gStruct{i}=find(gList(:,3)==i);
    end

end
