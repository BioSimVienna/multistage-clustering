function gListVMD = circularGraphWithVmdColoring(adjMat, gList, gListReference)

    % Plots a circular graph with VMD Coloring Scheme
    % 
    % Description:
    % ------------
    % Auxiliary function to plot circular plots in the same
    % colors as VMD plots
    %
    % Syntax:
    % -------
    % gListVMD = circularGraphWithVmdColoring(gList, adjMat)
    %
    % Variable:
    % ---------
    % adjMat         : [n n] adjacency matrix
    % gList          : [n 3] matrix with (objId, groupSize, groupIdx) in every row
    % gListReference : [n 3] matrix with (objId, groupSize, groupIdx) in every row used as reference for VMD coloring
    % gListVMD       : gList extended by VMD colors
    %
    % Method:
    % -------
    % Use Cibena-algorithm for median alignment
    % coloring of gList is done the same way as for gListReference
    %
    % Example:
    % --------
    % n/a
    %
    % Literatur:
    % ----------
    % n/a
    %

    % if no reference gList is given
    if nargin<3,
        gListReference = gList;
    end

    % Cibena-Algorithm for median aligment
    medians = [];
    A = gListReference;
    for i=1:20       % up to possible cluster colors
        B = A(A(:,3)==i,:);
        C = B(:,1,1);
        M = median(C);
        medians = [medians; M];
    end

    % circular plot
    draw_gList = CircularGraphDrawerColorsAppropriateVmdColors(gList, adjMat, 8, medians);
    
    % extended gList
    gListVMD = draw_gList.colorTableForVMD;

end
