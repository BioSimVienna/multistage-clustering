function [mapInt, dist] = mapInteresection(maps)

    % Calculates an intersection for a list of clustering results
    % 
    % Description:
    % ------------
    % This function evaluates how often two objects where assigned to the same cluster
    % among a list of clustering results
    %
    % Syntax:
    % -------
    % [mapInt, dist] = mapInteresection(maps)
    %
    % Variable:
    % ---------
    % maps   : [f n] list of clustering results with
    %          f number of clustering results
    %          n total number of objects in clusters
    % mapInt : [n n] symmetric matrix with pairwise count of how often two objects belong to the same cluster
    % dist   : [n n] distance matrix between objects defined as dist=f-mapInt
    %          distance is also called dissimilarity
    %
    % Method:
    % -------
    % 
    %
    % Example:
    % --------
    % n ... number of atoms in a protein structure, e.g. 826 atoms
    % d ... number of dimensions, usually 3
    % l ... length of a segement of a trajectory, e.g. 40 frames
    %
    % Literatur:
    % ----------
    % n/a
    %

    % number of clustering results
    s = size(maps);

    % allocate
    mapInt = zeros(s(2));

    % MatLab optimized
    for i = 1:s(1)
        mapInt = mapInt + bsxfun(@eq, maps(i,:),maps(i,:)');
    end
    
    if nargout == 2
        % dist ... position (i,j) is count how often the two items have not been in the same cluster    
        dist = s(1) - mapInt;
    end

end
