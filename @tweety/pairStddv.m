function [stddv, mdist] = pairStddv(traj)

    % Calculates the pairwise standard deviation matrix of a matrix list
    % 
    % Description:
    % ------------
    % Function to calculate the pairwise standard deviation of the distances between
    % points along a trajectory
    %
    % Syntax:
    % -------
    % [stddv,mdist] = pairStddv(traj)
    %
    % Variable:
    % ---------
    % traj  : [n d l] structure with
    %         n objects/points
    %         d dimensions
    %         l length of trajectory
    % stddv : [n n] stddv matrix between points along trajectory
    % mdist : [n n] mean distance matrix between points along trajectory
    %
    % Method:
    % -------
    % calculates for all pairs of points the standard deviation of distances along trajectory
    %
    % Example:
    % --------
    % n ... number of atoms in a protein structure, e.g. 826 atoms
    % d ... number of dimensions, usually 3
    % l ... length of a segement of a trajectory, e.g. 40 frames
    %
    % Literatur:
    % ----------
    % n/a
    %

    % Prevoius comments:
    % ------------------
    % NB 1: the frames do not necessarily have to be in cronological order, any set of frames is OK
    % NB 2: there is no need for normal distribution of the pair distances between atoms
    %       consequences of lacking normal distribution have to be accounted by the user
    % NB 3: order of dimensions of traj is already optimized for performance

    % matrix size
    s = size(traj);

    % allocate
    stddv = zeros(s(1));
    mdist = zeros(s(1));

    for i=1:s(1)
        for j=i+1:s(1)
            % distances between atoms
            d = reshape(traj(i,:,:)-traj(j,:,:),s(2),s(3));
            d = sqrt(sum(d.*d));
            stddv(i,j) = std(d);
            if nargout == 2
                mdist(i,j) = mean(d);
            end
        end
    end
    
    stddv = stddv+stddv';
    if nargout == 2
        mdist = mdist+mdist';
    end

end
