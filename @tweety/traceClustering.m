function [map, q] = traceClustering(S, c, r)

    % tr(xSx)-clustering of a distance matrix
    % 
    % Description:
    % ------------
    % Function minimizes the target function tr(x'Sx) to cluster objects
    % defined by a matrix of pairwise distances
    %
    % Syntax:
    % -------
    % [map,q] = traceClustering(S,c[,r])
    %
    % Variable:
    % ---------
    % S   : [n n] symmetric distance matrix
    %        distances does not necessarily require to follow a metric
    % c   : number of clusters
    % r   : seed for random startup position
    % map : cluster result
    % q   : value of target function
    %
    % Method:
    % -------
    % the n objects are at first randomly distrubuted among the c clusters
    % the iteration is done as following:
    %  - choose a random permutation of the n indices
    %  - go in turn thru all the permutated indices and move object to best cluster
    %  - note, if object has changed its current cluster
    %  - stop loop, if no object has changed its current cluster
    % The resulting object to cluster map is related to a LOCAL minimum of the target function
    % However, there is no garantee, that the GLOBAL minimum was achived
    % It is recommended to use traceClusteringWrapper to locate objects which are likely
    % to be in the same cluster for all LOCAL minima of the target function
    %
    % Example:
    % --------
    % n ... number of atoms in a protein structure, e.g. 826 atoms
    % S ... pairwise standard deviation matrix of a trajectory
    % c ... c=7 is a good choose for the protein above
    %
    % Literatur:
    % ----------
    % n/a
    %

    % Previous comments:
    % ------------------
    % NB: only one random initial constellation is used.
    %     If the target function is sensitive to chance
    %     the result is heavily dependent on this initial condition
    %     In most cases it is enough to get any decent clustering result


    % set seed for random number generation
    if nargin<3,
        rng default;
    else
        rng(r);
    end 

    % matrix size
    n = size(S, 1);

    % random initial condition
    map = randi([1,c], 1, n);

    % calculate target function
    x = sparse(1:n, map, 1);
    q = trace(x'*S*x);

    % systematical search
    hits = intmax;
    while hits > 0
    
        qPart = zeros(n, c);
        for col=1:n
            qPart(:, map(col))=qPart(:, map(col))+S(:,col);
        end

        % Minimalen qPart suchen
        [~, qPartMin] = min(qPart, [], 2);

        % search for improvements
        hits = 0;
        permute = randperm(n);
        for i1=1:n
            i=permute(i1);
            if(map(i) ~= qPartMin(i))
                % change qPart Anteile
                qPart(:, map(i))=qPart(:, map(i))-S(:,i);
                qPart(:, qPartMin(i))=qPart(:, qPartMin(i))+S(:,i);
                map(i) = qPartMin(i);
                [~, qPartMin] = min(qPart, [], 2);
                hits=hits+1;
            end
        end
        % fprintf('%i hits remaining\n', hits);
    end

    if nargout == 2
        % calculate target function again
        x = sparse(1:n, map, 1);
        q = trace(x'*S*x);
    end

end
