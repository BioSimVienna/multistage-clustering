classdef tweety

    % tweety data analysis package
    %
    % If you are reading this you have installed only these parts of tweety
    % which are required for spartio-temporal clustering of a trajectory
    % of a protein to find rigid domains.
    %
    % Best way to start is by adapting the sample batch file
    % according to your needs
    % 
    % Author: Mischa Kenn, michael@kenn.at
    % last change: 160215

    properties

    end

    properties (GetAccess='public', SetAccess='private')

    end

    properties (GetAccess='private', SetAccess='private')

    end

    methods

        %% constructor
        function obj = tweety()
        end

    end

    methods (Static)

        % --------------------------------
        % functons documented as of 160208
        % --------------------------------
        [gList, gStruct] = adjMat2groups(adjMat)
        [stddv, mdist]   = pairStddv(traj)
        [map, target]    = traceClustering(S, c, r)
        [mapInt, dist]   = mapIntersection(maps)

        gListVMD         = circularGraphWithVmdColoring(adjMat, gList, gListReference)

    end

end
