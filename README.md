# README for Matlab package tweety #

## Content ##

Software to carry out procedures as described in
Kenn et al., Spatiotemporal multistage consensus clustering in molecular dynamics studies of large proteins
The present source is sufficient to run the attached sample under Matlab R2014.

## Repository ##
To perform sample you will also need the trajectory which you can download from
http://www.meduniwien.ac.at/msi/biosim/multistageClusteringConsolidation/trajB4405.mat

## Installation ##

Installation is accomplished by simply copying files into the appropriate directories, as follows 
```
into @tweety directory:
	@tweety\tweety.m
	@tweety\adjMat2groups.m
	@tweety\mapIntersection.m
	@tweety\pairStddv.m
	@tweety\traceClustering.m
	'@tweety\circularGraphWithVmdColoring.m'
```

```
into current working directory or matlab path:
        CircularGraphDrawerColorsAppropriateVmdColors.m
        getVMDcolours.m
```
  
```      
to run the sample you need:
        sampleRunfileB4405.m
```

A detailed description of the individual files can be either found in 'sampleRunfileB4405.m' or by the command 'doc tweety' in Matlab.

## Method ##

This source performs multistage consensus clustering (MCC) for a trajectory.
The output provides groups of atoms which form rigid domains.
Rigid domains can be visualized by a circular plot.
The individual steps are documented in 'sampleRunfileB4405.m' in detail, so this is only an overview:
    1. choose trajectory
    2. build STDDV matrices of segments of trajectory
    3. trace-clustering (spatial clustering) of STDDV matrices
    4. temporal consolidation of spatial clustering results
    5. build adjacency matrix
    6. establish rigid domains
    7. visualization with circular plots

## Sample ##

We provide a 50 MB Molecular dynamics trajectory as test-data.
Load it into the MATLAB workspace as Variable "traj" before starting
Read and adapt the 'sampleRunfileB4405.m' according to your requirements

This sample is a 5022 frame trajectory of a protein with 826 C-alpha atoms.
The length of a frame is 50 ps, so the full trajectory covers about 250 ns.
It was calculated with GROMACS with an integration time of 2 fs (this gives 25000 integrations per frame).
In this sample 200 ns of the trajectory are split into 100 segments of 40 frames each.
Each segment is first clustered spatially and than consolidated temporally.
Rigid domains are build by C-alpha atoms belonging to the same cluster for at least 97 out of 100 segments.
Visualization is possible with two types of circular plots.

## Support ##

If you need help, please contact michael@kenn.at

## Feedback ##

We appreciate any kind of feedback.
Contact:
Michael Kenn
michael@kenn.at
CeMSIIS

## Copyright ##

This work of BioSimVienna is licensed under a Creative Commons Attribution 4.0 International License.
The license can be downloaded here: http://creativecommons.org/licenses/by/4.0/