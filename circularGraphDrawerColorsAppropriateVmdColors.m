classdef CircularGraphDrawerColorsAppropriateVmdColors < handle
    
    properties (Hidden)
        figureName;
        adjMatrix;  % the adjacence matrix for which the circular graph should be produced
        completeLink1;  % the completeLink1 clustering result
        vmdColours; % variable holding the color references appropriate to the vmd colors 
%         selectedColours = {'red'; 'green'; 'blue'; 'cyan'; 'pink'; 'yellow'; 'purple'; 'grey'}; % the selected colors for drawing the circular graph
%         selectedColours = {'red'; 'green'; 'blue'; 'cyan'; 'pink'; 'yellow'; 'purple'; 'tan'; 'violet'; 'grey'}; % the selected colors for drawing the circular graph
        selectedColours = {'violet'; 'red'; 'yellow'; 'pink'; 'purple'; 'green'; 'blue'; 'tan'; 'cyan'; 'grey'}; % the selected colors for drawing the circular graph
        mediansInput;
        mediansLocal;
        umrechnungsTabelle;
        numberOfClusters2Draw;
        numberOfNodesPerCluster;    % this variable is one column holding the counts of nodes per cluster (row 1 ... holding the number of nodes in cluster 1, ...). The last index holds all the clusters together which are not to be drawn.
        myColorMap;                 % the color input for the circular graph class
        myLabels;                    % the label input for the circular graph class
        
    end
    
    properties
        colorTableForVMD;
    end
    
    methods
        
        function CGD = CircularGraphDrawerColorsAppropriateVmdColors(gList, adjMat, numberOfClusters2DrawAsInt, mediansInput)      %function CGD = CircularGraphDrawerColorsAppropriateVmdColors(gList, adjMat, ClusterObjectFileReferenceAsStr, numberOfClusters2DrawAsInt, mediansInput)
            % initial ...
            CGD.vmdColours = getVMDcolours;
            CGD.mediansInput = mediansInput;
            CGD.numberOfClusters2Draw = numberOfClusters2DrawAsInt;
            
            name = inputname(1)       %name = names{1,1}
             %calculate medians for actual figure:
            CGD.mediansLocal = [];
            A = gList;  %A = cluster.results.completeLink1;
            for i=1:CGD.numberOfClusters2Draw
                B=A(A(:,3)==i,:);
                C=B(:,1,1);
                M = median(C);
                CGD.mediansLocal = [CGD.mediansLocal; M];
            end
            
            calculateMedianSpecificColorIndices(CGD);
            
            
            CGD.figureName = name;
            CGD.completeLink1 = gList;       
            CGD.colorTableForVMD = array2table(CGD.completeLink1);
            CGD.adjMatrix = adjMat;    
            setNumberOfNodesPerCluster(CGD);
            CGD.myColorMap = zeros(length(CGD.adjMatrix), 3);
            
            setlabels(CGD);
            drawCircularGraphInVmdColorsColoringBySize(CGD);
            
            [name ' printed']
        end
        
        function drawCircularGraphInVmdColorsColoringBySize(CGD)
           colorGroupNumber = zeros(CGD.numberOfClusters2Draw + 1,1);
           for i=1:length(CGD.adjMatrix)
                
                clustNum = CGD.completeLink1(i,3);
                if (clustNum > CGD.numberOfClusters2Draw)
                      clustNum = CGD.numberOfClusters2Draw + 1;
                end

                colorGroupNumber(clustNum) = colorGroupNumber(clustNum) + 1;

                
                
                if (clustNum >CGD.numberOfClusters2Draw)
                    tempColor = CGD.vmdColours.rgb.(CGD.selectedColours{length(CGD.selectedColours)});
                    cellArrayColorsForVMD{i,1} = CGD.selectedColours{length(CGD.selectedColours)};
                else
                    tempColor = CGD.vmdColours.rgb.(CGD.selectedColours{CGD.umrechnungsTabelle(clustNum)});
                    cellArrayColorsForVMD{i,1} = CGD.selectedColours{CGD.umrechnungsTabelle(clustNum)};
                end
                
                
                if (clustNum <= CGD.numberOfClusters2Draw)
                    for colorCounter =1:3
                        if (tempColor(colorCounter) < 0.5)
                            tempColor(colorCounter) = tempColor(colorCounter) + (colorGroupNumber(clustNum) / CGD.numberOfNodesPerCluster(clustNum)/3);
                        else
                            tempColor(colorCounter) = tempColor(colorCounter) - (colorGroupNumber(clustNum) / CGD.numberOfNodesPerCluster(clustNum)/3);
                        end
                    end
                end
                CGD.myColorMap(i,:) = tempColor;
                 
            end
            
            CGD.colorTableForVMD = [CGD.colorTableForVMD cell2table(cellArrayColorsForVMD)];
            
            CGD.drawAndSaveFigure();
        end
        
        
        
        function calculateMedianSpecificColorIndices(CGD)
           for locMedIndex=1: CGD.numberOfClusters2Draw
               actualLowestDistance = 10000;
               actualColorIndex = 100;
%                        'in here 1'
               for globMedIndex = 1:length(CGD.mediansInput)
%                        'in here 2'
                   actualDistance = abs(CGD.mediansInput(globMedIndex) - CGD.mediansLocal(locMedIndex));
                   if (actualDistance < actualLowestDistance)
                       actualLowestDistance = actualDistance;
                       actualColorIndex = globMedIndex;
%                        'in here 3'
                   end
               end
               CGD.umrechnungsTabelle = [CGD.umrechnungsTabelle; actualColorIndex];
           end
        end
        
    end
    
    methods (Hidden)
        
        % this methods produces a result with one column holding the counts of nodes per cluster (row 1 ... holding the number of nodes in cluster 1, ...). The last index holds all the clusters together which are not to be drawn.
        % the cluster number one is still the biggest cluster ...
        function setNumberOfNodesPerCluster(CGD)
            finalColorGroupNumber = zeros(CGD.numberOfClusters2Draw + 1,1);
            for i=1:length(CGD.adjMatrix)
                clustNum = CGD.completeLink1(i,3);
                if (clustNum > CGD.numberOfClusters2Draw)
                    clustNum = CGD.numberOfClusters2Draw + 1;
                end
                finalColorGroupNumber(clustNum) = finalColorGroupNumber(clustNum) + 1;
            end
            CGD.numberOfNodesPerCluster = finalColorGroupNumber;
        end
        
        function setlabels(CGD)
            CGD.myLabels = cell(length(CGD.adjMatrix)); %passt das hier?
            for i=1:length(CGD.adjMatrix)
                if(mod(i,59)==0)
                    CGD.myLabels{i}=num2str(i);
                else
                    CGD.myLabels{i}='';
                end
            end
        end
        
        function drawAndSaveFigure(CGD)
            f = figure('Name',CGD.figureName,'NumberTitle','off');
            
            circG = circularGraph(CGD.adjMatrix,'Colormap',CGD.myColorMap,'Label', CGD.myLabels);

            % with the following code fragment the parts not within a cluster will be grayed out and moved to the background
            for i=1:length(CGD.adjMatrix)
                clustNum = CGD.completeLink1(i,3);
                if (clustNum > CGD.numberOfClusters2Draw)
                    circG.Node(1,i).Visible = false;
                end
            end
            for i=1:length(CGD.adjMatrix)
                clustNum = CGD.completeLink1(i,3);
                if (clustNum <= CGD.numberOfClusters2Draw)
                    circG.Node(1,i).Visible = true;
                end
            end
           print(f, '-dpng',CGD.figureName,'-r300')
        end
        
    end
    
end

