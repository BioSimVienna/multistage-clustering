function colours = getVMDcolours()
%GETVMDCOLOUR 
% Each clour in VMd is assigned to a number.  
% Example: colours = getVMDcolours() gives back a structure with colour 
% names assigned to the corresponding numbers in VMD.

% Colours by name
colours.name.blue        = 0;
colours.name.red        = 1;
colours.name.grey       = 2;
colours.name.orange     = 3;
colours.name.yellow     = 4;
colours.name.tan        = 5;
colours.name.silver     = 6;
colours.name.green      = 7;
colours.name.white      = 8;
colours.name.pink       = 9;
colours.name.cyan       = 10;
colours.name.purple     = 11;
colours.name.lime       = 12;
colours.name.mauve      = 13;
colours.name.ochre      = 14;
colours.name.iceblue    = 15;
colours.name.black      = 16;
colours.name.yellow2    = 17;
colours.name.yellow3    = 18;
colours.name.green2     = 19;
colours.name.green3     = 20;
colours.name.cyan2      = 21;
colours.name.cyan3      = 22;
colours.name.blue2      = 23;
colours.name.blue3      = 24;
colours.name.violet     = 25;
colours.name.violet2    = 26;
colours.name.magenta    = 27;
colours.name.magenta2   = 28;
colours.name.red2       = 29;
colours.name.red3       = 30;
colours.name.orange2    = 31;
colours.name.orange3    = 32;

% Colours by numbers
colours.number.no0      = 'blue';
colours.number.no1      = 'red';
colours.number.no2      = 'grey';
colours.number.no3      = 'orange';
colours.number.no4      = 'yellow';
colours.number.no5      = 'tan';
colours.number.no6      = 'silver';
colours.number.no7      = 'green';
colours.number.no8      = 'white';
colours.number.no9      = 'pink';
colours.number.no10     = 'cyan';
colours.number.no11     = 'purple';
colours.number.no12     = 'lime';
colours.number.no13     = 'mauve';
colours.number.no14     = 'ochre';
colours.number.no15     = 'iceblue';
colours.number.no16     = 'black';
colours.number.no17     = 'yellow2';
colours.number.no18     = 'yellow3';
colours.number.no19     = 'green2';
colours.number.no20     = 'green3';
colours.number.no21     = 'cyan2';
colours.number.no22     = 'cyan3';
colours.number.no23     = 'blue2';
colours.number.no24     = 'blue3';
colours.number.no25     = 'violet';
colours.number.no26     = 'violet2';
colours.number.no27     = 'magenta';
colours.number.no28     = 'magenta2';
colours.number.no29     = 'red2';
colours.number.no30     = 'red3';
colours.number.no31     = 'orange2';
colours.number.no32     = 'orange3';

% RGB definition (approxmated) 
colours.rgb.blue     = [0    0    1];
colours.rgb.red      = [1    0    0];
colours.rgb.grey     = [0.50 0.50 0.50];
colours.rgb.orange   = [1    0.65 0];
colours.rgb.yellow   = [1    1    0];
colours.rgb.tan      = [0.82 0.70 0.55];
colours.rgb.silver   = [0.40 0.40 0.40];
colours.rgb.green    = [0    1    0];
colours.rgb.white    = [1    1    1];
colours.rgb.pink     = [1    0.75    0.80];
colours.rgb.cyan     = [0    1       1];
colours.rgb.purple   = [0.63 0.13    0.94];
colours.rgb.no12     = 'lime';
colours.rgb.no13     = 'mauve';
colours.rgb.no14     = 'ochre';
colours.rgb.no15     = 'iceblue';
colours.rgb.no16     = 'black';
colours.rgb.no17     = 'yellow2';
colours.rgb.no18     = 'yellow3';
colours.rgb.no19     = 'green2';
colours.rgb.no20     = 'green3';
colours.rgb.no21     = 'cyan2';
colours.rgb.no22     = 'cyan3';
colours.rgb.no23     = 'blue2';
colours.rgb.no24     = 'blue3';
colours.rgb.violet   = [0.93 0.51 0.93];
colours.rgb.no26     = 'violet2';
colours.rgb.no27     = 'magenta';
colours.rgb.no28     = 'magenta2';
colours.rgb.no29     = 'red2';
colours.rgb.no30     = 'red3';
colours.rgb.no31     = 'orange2';
colours.rgb.no32     = 'orange3';

end 