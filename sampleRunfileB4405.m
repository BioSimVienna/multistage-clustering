% This is a sample file of how to apply spatio-temporal clustering
% to a trajectory

% copy this file to the Matlab root directory and ensure to have
% installed following functions of tweety into @tweety
1. tweety constructor
2. tweety.pairStddv
3. tweety.traceClustering
4. tweety.mapIntersection
5. tweety.adjMat2groups
% (this installation will in most cases be just copying the files)

% to run this sample you will need the trajectory
% provided in SampleData/trajB4405.mat
% traj is a [n 3 l] matrix for n atoms in 3 dimensions for a trajectory of length l
% for this sample n=826 and l=5022, but only 4000 frames are used (some start-up is pruned)
% we split the trajectory into 100 segments of 40 frames length each
% in this sample the first segment are frames 522-561, the second segment are frames 562-581, and so on
% the last segment are consequently frames 4502-4521

% number of atoms n=826
n=size(traj,1);
% allocate 100 stddv matrices, for each segment one
stddv = zeros(n,n,100);
% calculate stddv matrices
% this will take about 30-40 minutes
% you can use parpool for speed-up 
for i=1:100
    stddv(:,:,i)=tweety.pairStddv(traj(:,:,522+(i-1)*40:522+i*40-1));
end
% Elapsed time is 2182.320677 seconds.

% now cluster each stddv matrix spatially with target function q=tr(x'Sx)
maps=zeros(100,n);
for i=1:100
    maps(i,:)=tweety.traceClustering(stddv(:,:,i),7);
end

% NB: with this approach we only get cluster results associated
%     with LOCAL minima of the target function.
%     There are two different ways to deal with this issue
%         1. perform traceClustering a couple of times (e.g. 100) and only choose the best result
%         2. perform traceClustering a couple of times (e.g. 100) and only consider objects to be
%            in the same cluster which are in the same cluster for all local minima of the target function
%     In this sample we skip this part, but in practice we recommend to adjust the procedure suitable 
%     for the data.

% next step is temporal clustering
% build dissimilarity matrix by counting, how often two object are not in the same cluster
[~,dissimilarity]=tweety.mapIntersection(maps);

% choose a threshold for dissimilarity
% two atoms are connected if dissimilarity is below threshold
maxDissimilarity = 3;
adjMat = (dissimilarity <= maxDissimilarity)*1;

% to find groups of atoms which build rigid domains
% we are looking for connected components in the graph specified by adjMat
[gList, gStruct] = tweety.adjMat2groups(adjMat);

% To show results, download and install the circularGraph toolbox from
% http://www.mathworks.com/matlabcentral/fileexchange/48576-circulargraph
circularGraph(adjMat)
% alternatively you can use our internal function to
% plot in colors consistent with VMD
% this function requires 2 more files in you working directory:
% CircularGraphDrawerColorsAppropriateVmdColors.m
% getVMDcolours.m
gListVMD = tweety.circularGraphWithVmdColoring(adjMat, gList, gList);
